# Description de notre langage
Notre langage sert à décrire une API REST. De plus, avec l'IDE il y a une génération automatique des requêtes curl et en python. Dans le cas de ce dernier, le *framework* Flask est utilisé et nous générons deux fichiers. Le premier est un *template* serveur (un *backend* implémentant l'API décrite) et le second est un *template* de tests de ce même serveur (un test par requête). Enfin, les *plugins* de l'IDE ont été améliorés afin de rendre l'expérience utilisateur plus agréable.
## Description de la grammaire
Sur ce répertoire, un diagramme de classes présente le modèle initial de la grammaire. Cependant, cette dernière a évolué au fur-et-à-mesure de sa rédaction avec *Xtext*. Dans cette section, la grammaire va donc être succinctement expliquée. L'API REST possède une configuration, un ensemble d'objets et un ensemble de requêtes.

Comme son nom l'indique, une configuration permet à l'utilisateur d'éditer les différents paramètres de l'API qu'il souhaite décrire (comme son port, son adresse) mais aussi des paramètres propres à notre langage et l'IDE (indiquer par exemple le langage pour la génération de code ; ici cela ne concerne que python et JavaScript, bien que seul python soit implémenté à ce jour...).

Un objet permet de factoriser la description des structures de données qui seront souvent utilisées dans les requêtes. De manière à calquer le corps des requêtes et réponses classiques en Web, un objet peut être vu comme un ensemble de paires clé-valeur. Néanmoins, il a aussi fallu considérer les cas plus complexes où la valeur est en réalité un ensemble. Ainsi, un objet a un nom et un ensemble de parties. Une partie a une clé, un type (un primitif classique ou un autre objet) et une valeur précisant s'il s'agit d'un ensemble ou non.

Une requête se compose d'éléments nécessaires et d'autres optionnels. Par définition, une requête a tout d'abord un verbe (REST) et un URI. Puis, afin d'enrichir la description d'une requête, cette dernière peut se composer d'un ensemble de *headers*, un ensemble de *status*, un corps (le body de la requête) et d'un corps de retour (celui de la réponse attendue). Les *headers* et les *status* sont des listes de clé-valeur. Concernant les corps, comme expliquer précédemment, c'est là que l'utilisateur va pouvoir mettre à profits les objets préalablement décrits afin de préciser avec commodité et rapidité leur contenu : un corps se compose donc d'un ensemble d'objets.

Cette description informelle présente seulement les généralités du modèle : il convient ici de soutenir le lecteur lors de sa révision de la grammaire définie sur *Xtext*. Par exemple, une requête - comme un objet - peut avoir un commentaire (lui-même étant une chaîne de caractères, un élément terminal du langage etc.).
## Description de la syntaxe concrète
La syntaxe concrète suit le schéma de la section précédente, avec dans l'ordre :
- La configuration de l'API.
- Description des objets utilisés dans les requêtes.
- Description des requêtes.

Les exemples qui suivront seront indentés avec des retours à la ligne : ce n'est pas une contrainte du langage mais plutôt pour une aide pour le lecteur.
### Syntaxe de la configuration
Ce sont des clé-valeur (la clé étant séparée de la valeur par `:`) et chacune des paires se termine par un `;`. Voici un exemple :
```
address: localhost;
port: 5000;
comment: "a comment of the following API";
language: python;
```
Aucune de ces clés n'est obligatoire. De plus, il est possible de les écrire dans n'importe quel ordre.
### Syntaxe pour déclarer des objets
Cette section commence par la déclaration `objects` puis de `:`. S'ensuivent autant de définitions d'objet que souhaité. Une telle définition commence par le nom de l'objet, un commentaire (optionnel, introduit et finit par `%`) puis l'ensemble de ses champs (dont l'énumération est précédée d'un `:`). La liste de champs a pour séparateur `,` et se finit par un `;`. Un champ se décrit avec son nom puis de son type (encadré des caractères `<` et `>`). Enfin, l'on précise s'il s'agit d'un ensemble en accolant directement au type (au sein des chevrons donc) l'annotation classique `[]`. Voici un exemple d'objets :
```
objects:
    Person :
        name<string>,
        id<int>;

    Pet % representation of a pet % :
        name<string>,
        id<int>,
        owner<Person>;
```
Il est à noter que si l'on souhaite utiliser des objets dans d'autres (des *Nested objects*), il est nécessaire de définir l'objet encapsulé au préalable. Dans cet exemple, le champ *owner* d'un l'objet *Pet* est de type *Person*, dont la définition existe.
### Syntaxe pour déclarer des requêtes
Une requête commence par son verbe REST puis son URI. Pour cette dernière, il est possible d'y ajouter des paramètres avec la syntaxe `<nom:type>`. Le reste est alors optionnel. Pour continuer la définition, un commentaire peut être inséré puis `:` est utilisé pour enrichir la requête avec des *headers*, des *status* ainsi que les corps (de la requête elle-même et celui attendu dans la réponse). Ces quatre champs peuvent être écrits dans n'importe quel ordre et suivent une déclaration similaire, à savoir :
- le nom (*headers*, *responses*, *request_body* ou *response_body*).
- `:`.
- `{` le contenu du champ `}`.

Pour le contenu, soit ce sont des clé-valeur de chaînes de caractères (*headers* et *responses*, les status) que l'on écrit classiquement `"clé":"valeur"`, chacune des paires étant séparée par `,` ; soit ce sont des corps et - tout comme les objets - l'on sépare chaque champ par `,` et un champ se décrit par le motif  `"nom"<"type">` ou `"nom"<"type"[]>`. Par exemple :
```
requests:
    GET /test/<worksFine:boolean>;

    GET /connect/ % to log in % :
        headers: {"connect-sso":"true", "content/type":"json"}
        responses: {"200":"logged in"};

    POST /home/<id:int> % another exemple with POST verb % :
        headers: {"connect-sso":"true"}
        responses: {"200":"No problem", "403":"No right"};

    GET /pets/<pet-id:int> :
        response_body: {pet<Pet>};

    POST /pets:
        request_body: {pets<Pet[]>};

    PUT /pet/<pet-id:string>/<new-name:string> % change the name of the pet %;

    PUT /pet/<pet-id:string> % change the name of the pet % :
    	request_body: {name<string>}
    	response_body: {pet<Pet>};
```
## Les différents codes générés
Le code python généré contient les *handlers* des requêtes en utilisant le *framework* Flask. De plus, on vérifie automatiquement les types des paramètres dans l'URI, et l'on prépare les reponses du *body*. Ce code contient également les classes python des objets définis dans la grammaire. Ceci permet de ne pas créer les DTO de l'API soi-même. Ce code peut servir de bases pour l'implémentation de l'API : par exemple, dans le but de rapidement tester l'ergonomie de celle-ci (les réponses sont-elles satisfaitantes ? etc.).

Un *template* de tests en utilisant le *framework* pytest est également généré. Celui-ci permet de simplifier l'implémentation de l'API en n'ayant qu'à remplir le code des tests sans se soucier de la structure du fichier.

Enfin, le dernier fichier généré est un fichier `.txt` contenant des requêtes curl pour tester l'API. Toutes les requêtes ne sont pas directement fonctionnelles cependant. En effet, il faut saisir les paramètres de l'URI quand il y en a. L'utilité de telles commandes peut être remis en cause. Malgré tout, il existe des cas où elles pourraient être utiles : si le langage est utilisé pour décrire une API déjà existante, ces dernières permettraient alors de tester le serveur implémentant l'API.

Il est à noter que la génération python suppose que l'utilisateur soit alerte sur certaines problématiques. En effet, il ne faut pas utiliser les mots clés propres à ce langage afin  d'éviter des erreurs inattendues.
## Fonctionnalités de l'IDE
Dans cette section, nous abordons les fonctionnalités ajoutées (ou enrichies) dans l'IDE.
### Validation
Deux requêtes identiques ne sont pas possibles (que ce soit vis-à-vis de la grammaire ou du code généré, ici en python). La validation concerne également l'unicité d'autres éléments. Certaines sont bloquantes, comme lorsqu'un objet ou un *payload* ont le même nom. Voici quelques exemples :
```
requests:
    GET /pets/<pet-id:int> :
        response_body: {pet<Pet>, pet<int>};
```
```
objects:
    Person :
        name<string>,
        name<int>;
```
Une autre validation bloquante concerne l'unicité du nom des objets, comme ci-dessous :
```
objects:
    Person :
        id<int>;
    Person :
        name<string>;
```
D'autres validations vérifient les clé-valeur, comme dans les *headers* et les *status* où il ne faut pas qu'apparaissent pas deux fois une même clé dans une même requête. Cette vérification ne renvoit qu'un *warning* (donc non bloquant pour la création des fichiers). On peut voir un exemple de ces deux cas ci-dessous.
```
requests:
	POST /:
		headers: {"connect-sso":"true", "connect-sso":"true"}
		responses: {"200":"No problem","200":""};
```
La dernière validation consiste à vérifier que les clés présentes dans les *status* d'une réponse sont bien des entiers. Il s'agit seulement d'un *warning*.
```
requests:
	POST /:
		responses: {"2A0":""};
```
### Outline
Celle-ci décrit (sous forme d'arborescence) toutes les sections et sous-sections des objets et des requêtes. Certaines propriétés (comme les commentaires) sont volontairement omises afin de ne pas surcharger l'affichage.
### Auto-complétion
Partiellement implémentée, elle concerne à ce jour le type primitif des paramètres des URI ainsi que le verbe des requêtes. On note cependant que d'autres éléments syntaxiques sont auto-complétés par les *plugins* déjà présent dans l'IDE.
### Highligting
Cette fonctionnalité a été largement travaillé et revisité. En effet, il nous a semblé important d'améliorer le rendu des différents éléments syntaxiques et ce, dans le but d'améliorer le confort de l'utilisateur. Un langage qui n'est pas utilisé est un langage mort (et surtout inutile dans notre cas).

Ainsi, comme cela sera visible lors du lancement de l'IDE pour notre langage, différentes couleurs et autres styles ont été appliqués sur un ensemble d'éléments syntaxiques.

# Rapport du groupe
Dans cette partie, nous allons expliquer l'organisation interne de l'équipe. En outre, nous verrons que plusieurs fonctionnalités de Gitlab (gestion des sources, *CI*, *issues*) ont été utilisées.
## Méthode de travail
Nous allons détailler ici comment nous avons travaillé pour ce projet.
### Méthode Kanban
Nous avons utilisé la méthode agile kanban pour mener à bien le projet. Pour cela, nous avons utilisé les *issues* Gitlab pour définir des tâches. Ensuite, nous avons utilisé les *labels* (*todo*, *doing* et *done*) du tableau des *issues* pour définir les tâches à faire, en cours et finies. De plus, un label *testing* a été ajouté afin de prévenir les autres membres de l'équipe que l'on attend une validation de leur part.
### Test du code
Afin de s'assurer de la bonne santé du projet, des tests ont été mis en place :
- test de la grammaire (en vérifiant que, pour un fichier d'entrée `.rest` donné, la grammaire est correctement interprétée).
- tests des fichiers générés (en vérifiant que, pour une entrée donnée, les fichiers générés sont corrects).
### Utilisation du Git
Chaque tâche a été implémentée sur une branche dédiée. Aussi, les tests décrits précédemment sont systématiquement exécutés (grâce à la *CI* de Gitlab) lors d'un *push*. Aussi, d'autres règles ont été définies au sein du groupe : par exemple, tout *merge* d'une fonctionnalité sur la branche *dev* doit être validée par au moins un autre membre de l'équipe.
### Intégration continue
Le projet a été développé avec une intégration continue. En effet, nous avons travaillé sous forme de cycles itératifs. À chaque cycle, une version de la grammaire était définie. Cela nous a permis - avec notamment le soutien des tests de la *CI* - qu'à l'intérieur de chaque cycle nous avions une grammaire et une syntaxe valides, avec un IDE fonctionnel. Au fur-et-à-mesure des cycles, la grammaire du langage et l'IDE se sont ainsi enrichis et ce, avec continuité.
## Répartition du travail
Dans cette dernière partie, nous abordons la répartition des tâches. Cette répartition a été faite selon l'envie de chacun (au volontariat). Ci-dessous un résumé de la participation de chacun pour chaque cycle de travail :

grammaire version 1 :
- Guénaël : écriture de la grammaire, génération python (*template* du serveur), écriture des tests du fichier généré python
- Jordan :
- Quentin : *outline*, génération curl, tests du fichier généré curl

grammaire version 2 :
- Guénaël : mise à jour de la grammaire, mise à jour des tests curl et python (*template* du serveur et *template* des tests), auto-complétion, *validation* (unicité des URI propre à python)
- Jordan : tests de la grammaire
- Quentin : *validation* (unicité des URI propre au langage), *highlighting*, auto-complétion

grammaire version 3 :
- Guénaël : mise à jour de la grammaire, mise à jour python (*template* du serveur), *outline*, *validation*, tests de la grammaire
- Jordan : tests de la grammaire, *highlighting*
- Quentin : correction de la grammaire, *outline*, *highlighting*, auto-complétion

Enfin, ce rapport a été rédigé Quentin, avec l'aide de Guénaël.