package coloring;

import org.eclipse.xtext.nodemodel.ICompositeNode;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultSemanticHighlightingCalculator;
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightedPositionAcceptor;
import org.eclipse.xtext.ui.editor.syntaxcoloring.ISemanticHighlightingCalculator;

import com.google.inject.Inject;

import fr.insarennes.dsl.services.RestGrammarAccess;

/**
 * Highlighing generator whose research is based on the model (and not on a of EObjects !). It lets us
 * comfortably access exactly what we want to highlight
 */
public class RestSemanticHighlightingCalculator extends DefaultSemanticHighlightingCalculator {
	
	@Inject
	RestGrammarAccess ga;
	
	@Override
	protected void doProvideHighlightingFor(XtextResource resource, IHighlightedPositionAcceptor acceptor) {
		ICompositeNode rootNode = resource.getParseResult().getRootNode();
		
		for (INode node : rootNode.getAsTreeIterable()) {
			// checks keyword or ruleCall and then applies styles
			if (node.getGrammarElement() == ga.getRequestAccess().getVerbVERBTerminalRuleCall_0_0()) {
				acceptor.addPosition(node.getOffset(), node.getLength(), RestHighlightingConfiguration.VERB_ID);
			} else if (node.getGrammarElement() == ga.getObjectAccess().getClass_nameMY_STRINGTerminalRuleCall_0_0()
					|| node.getGrammarElement() == ga.getObjectPartAccess().getField_typeOther_TypeParserRuleCall_2_0_1()) {
				acceptor.addPosition(node.getOffset(), node.getLength(), RestHighlightingConfiguration.OBJECT_ID);
			} else if (node.getGrammarElement() == ga.getParameterURIAccess().getTypeTypeParserRuleCall_3_0()
					|| node.getGrammarElement() == ga.getObjectPartAccess().getField_typeTypeParserRuleCall_2_0_0()) {
				acceptor.addPosition(node.getOffset(), node.getLength(), RestHighlightingConfiguration.URITYPE_ID);
			} else if (node.getGrammarElement() == ga.getRequestAccess().getCommentMY_COMMENTTerminalRuleCall_2_0()
					|| node.getGrammarElement() == ga.getConfigurationAccess().getCommentSTRINGTerminalRuleCall_1_2_2_0()
					|| node.getGrammarElement() == ga.getObjectAccess().getCommentMY_COMMENTTerminalRuleCall_1_0()) {
				acceptor.addPosition(node.getOffset(), node.getLength(), RestHighlightingConfiguration.COMMENTS_ID);
			} else if (node.getGrammarElement() == ga.getRestAPIAccess().getObjectsKeyword_1_0()
					|| node.getGrammarElement() == ga.getRestAPIAccess().getRequestsKeyword_2_0()) {
				acceptor.addPosition(node.getOffset(), node.getLength(), RestHighlightingConfiguration.BOLD_ID);
			} else if (node.getGrammarElement() == ga.getObjectPartAccess().getField_nameMY_STRINGTerminalRuleCall_0_0()) {
				acceptor.addPosition(node.getOffset(), node.getLength(), RestHighlightingConfiguration.NAME_ID);
			} else if (node.getGrammarElement() == ga.getParameterURIAccess().getVar_nameMY_STRINGTerminalRuleCall_1_0()) {
				acceptor.addPosition(node.getOffset(), node.getLength(), RestHighlightingConfiguration.ITALIC_ID);
			} else if (node.getGrammarElement() == ga.getConfigurationAccess().getAddressConfigAddressConfigParserRuleCall_1_0_2_0()
					|| node.getGrammarElement() == ga.getConfigurationAccess().getPortConfigPortConfigParserRuleCall_1_1_2_0()) {
				acceptor.addPosition(node.getOffset(), node.getLength(), RestHighlightingConfiguration.PARAM_ID);
			}
		}
		super.doProvideHighlightingFor(resource, acceptor);
	}
	
}
