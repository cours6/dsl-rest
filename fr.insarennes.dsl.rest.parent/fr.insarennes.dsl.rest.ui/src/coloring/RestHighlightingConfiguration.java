package coloring;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultHighlightingConfiguration;
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfigurationAcceptor;
import org.eclipse.xtext.ui.editor.utils.TextStyle;

public class RestHighlightingConfiguration extends DefaultHighlightingConfiguration {
	
	/* styles IDs */
	public static final String VERB_ID = "verb";
	public static final String URITYPE_ID = "uriType";
	public static final String OBJECT_ID = "object";
	public static final String COMMENTS_ID = "comments";
	public static final String BOLD_ID = "bold";
	public static final String ITALIC_ID = "italic";
	public static final String NAME_ID = "name";
	public static final String PARAM_ID = "param";
	
	public void configure(IHighlightingConfigurationAcceptor acceptor) {
		/* styles registration */
		acceptor.acceptDefaultHighlighting(VERB_ID, "Verb", verbTextStyle());
		acceptor.acceptDefaultHighlighting(URITYPE_ID, "UriType", typeTextStyle());
		acceptor.acceptDefaultHighlighting(OBJECT_ID, "Object", objectsTextStyle());
		acceptor.acceptDefaultHighlighting(COMMENTS_ID, "Comments", commentsTextStyle());
		acceptor.acceptDefaultHighlighting(BOLD_ID, "Bold", boldTextStyle());
		acceptor.acceptDefaultHighlighting(NAME_ID, "Name", nameTextStyle());
		acceptor.acceptDefaultHighlighting(ITALIC_ID, "Italic", italicTextStyle());
		acceptor.acceptDefaultHighlighting(PARAM_ID, "Param", paramTextStyle());
	}
	
	/* styles */
	
	public TextStyle verbTextStyle() {
		TextStyle textStyle = new TextStyle();
		textStyle.setColor(new RGB(120, 0, 200));
		textStyle.setStyle(SWT.BOLD);
		return textStyle;
	}

	
	public TextStyle typeTextStyle() {
		TextStyle textStyle = new TextStyle();
		textStyle.setColor(new RGB(55, 90, 200));
		textStyle.setStyle(SWT.ITALIC);
		return textStyle;
	}
	
	public TextStyle objectsTextStyle() {
		TextStyle textStyle = new TextStyle();
		textStyle.setColor(new RGB(200, 60, 120));
		textStyle.setStyle(SWT.BOLD);
		return textStyle;
	}
	
	public TextStyle commentsTextStyle() {
		TextStyle textStyle = new TextStyle();
		textStyle.setColor(new RGB(0, 150, 00));
		return textStyle;
	}
	
	public TextStyle boldTextStyle() {
		TextStyle textStyle = new TextStyle();
		textStyle.setStyle(SWT.BOLD);
		return textStyle;
	}
	
	public TextStyle nameTextStyle() {
		TextStyle textStyle = new TextStyle();
		textStyle.setColor(new RGB(150, 100, 30));
		return textStyle;
	}
	
	public TextStyle italicTextStyle() {
		TextStyle textStyle = new TextStyle();
		textStyle.setStyle(SWT.ITALIC);
		return textStyle;
	}
	
	public TextStyle paramTextStyle() {
		TextStyle textStyle = new TextStyle();
		textStyle.setColor(new RGB(89, 98, 117));
		textStyle.setStyle(SWT.BOLD);
		return textStyle;
	}
}