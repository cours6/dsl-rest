package fr.insarennes.dsl.tests

import com.google.inject.Inject
import fr.insarennes.dsl.generator.RestGenerator
import fr.insarennes.dsl.rest.RestAPI
import org.eclipse.xtext.generator.InMemoryFileSystemAccess
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith
/**
 * Class to test the creation of the python server file
 */
@ExtendWith(InjectionExtension)
@InjectWith(typeof(RestInjectorProvider))
class ToPyTest {
	
	var int fileNumber = 3;
	
	@Inject RestGenerator generator
	
	@Inject ParseHelper<RestAPI> parseHelper
	
	@Test
	def void test1() {
		val model = parseHelper.parse('''requests: GET /test % get %;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.py"),
			'''from flask import Flask
from distutils.util import strtobool
from json import JSONEncoder

app = Flask(__name__)
URL_PREFIX = ''
HOST='0.0.0.0'
PORT=5000
"""
% get %
"""
@app.route(URL_PREFIX + '/test', methods=['GET'])
def get_test():
	#TODO
	
	return  {"test":3}


class MyEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

if __name__ == "__main__":
	app.run(debug=True, host=HOST, port=PORT)
'''.toString
		)
	}
	
	@Test
	def void test2() {
		val model = parseHelper.parse('''
requests:
GET /test;
POST /
% 
post 
%;

POST /aze/aze-123AZ;

POST /a { "allow-origin":"*" };

POST /a-b { "allow-origin":"*" };

POST /aae/aze-123AZ { "Key":"Value","Key":"Value"};''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.py"),
			'''from flask import Flask
from distutils.util import strtobool
from json import JSONEncoder

app = Flask(__name__)
URL_PREFIX = ''
HOST='0.0.0.0'
PORT=5000
@app.route(URL_PREFIX + '/test', methods=['GET'])
def get_test():
	#TODO
	
	return  {"test":3}
"""
% 
post 
%
"""
@app.route(URL_PREFIX + '/', methods=['POST'])
def post():
	#TODO
	
	return  {"test":3}
@app.route(URL_PREFIX + '/aze/aze-123AZ', methods=['POST'])
def post_aze_aze123AZ():
	#TODO
	
	return  {"test":3}
@app.route(URL_PREFIX + '/a', methods=['POST'])
def post_a():
	#TODO
	
	return  {"test":3}
@app.route(URL_PREFIX + '/a-b', methods=['POST'])
def post_ab():
	#TODO
	
	return  {"test":3}
@app.route(URL_PREFIX + '/aae/aze-123AZ', methods=['POST'])
def post_aae_aze123AZ():
	#TODO
	
	return  {"test":3}


class MyEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

if __name__ == "__main__":
	app.run(debug=True, host=HOST, port=PORT)
'''.toString
		)
	}
	
	@Test
	def void test3() {
		val model = parseHelper.parse('''address:localhost; port:4300; requests: GET /test;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.py"),
			'''from flask import Flask
from distutils.util import strtobool
from json import JSONEncoder

app = Flask(__name__)
URL_PREFIX = ''
HOST='0.0.0.0'
PORT=4300
@app.route(URL_PREFIX + '/test', methods=['GET'])
def get_test():
	#TODO
	
	return  {"test":3}


class MyEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

if __name__ == "__main__":
	app.run(debug=True, host=HOST, port=PORT)
'''.toString
		)
	}
	
	@Test
	def void test4() {
		val model = parseHelper.parse('''requests: GET /<var:boolean>;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.py"),
			'''from flask import Flask
from distutils.util import strtobool
from json import JSONEncoder

app = Flask(__name__)
URL_PREFIX = ''
HOST='0.0.0.0'
PORT=5000
@app.route(URL_PREFIX + '/<var>', methods=['GET'])
def get_var(var):
	try:
		var = strtobool(var) == 1
	except Exception:
		return "", 404
	#TODO
	
	return  {"test":3}


class MyEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

if __name__ == "__main__":
	app.run(debug=True, host=HOST, port=PORT)
'''.toString
		)
	}
	
	@Test
	def void test5() {
		val model = parseHelper.parse('''requests: GET /<var:string>;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.py"),
			'''from flask import Flask
from distutils.util import strtobool
from json import JSONEncoder

app = Flask(__name__)
URL_PREFIX = ''
HOST='0.0.0.0'
PORT=5000
@app.route(URL_PREFIX + '/<var>', methods=['GET'])
def get_var(var):
	#TODO
	
	return  {"test":3}


class MyEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

if __name__ == "__main__":
	app.run(debug=True, host=HOST, port=PORT)
'''.toString
		)
	}
	
	@Test
	def void test6() {
		val model = parseHelper.parse('''requests: GET /<var:int>;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.py"),
			'''from flask import Flask
from distutils.util import strtobool
from json import JSONEncoder

app = Flask(__name__)
URL_PREFIX = ''
HOST='0.0.0.0'
PORT=5000
@app.route(URL_PREFIX + '/<var>', methods=['GET'])
def get_var(var):
	try:
		var = int(var)
	except Exception:
		return "", 404
	#TODO
	
	return  {"test":3}


class MyEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

if __name__ == "__main__":
	app.run(debug=True, host=HOST, port=PORT)
'''.toString
		)
	}
	
	@Test
	def void test7() {
		val model = parseHelper.parse('''requests: GET /<var:double>;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.py"),
			'''from flask import Flask
from distutils.util import strtobool
from json import JSONEncoder

app = Flask(__name__)
URL_PREFIX = ''
HOST='0.0.0.0'
PORT=5000
@app.route(URL_PREFIX + '/<var>', methods=['GET'])
def get_var(var):
	try:
		var = float(var)
	except Exception:
		return "", 404
	#TODO
	
	return  {"test":3}


class MyEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

if __name__ == "__main__":
	app.run(debug=True, host=HOST, port=PORT)
'''.toString
		)
	}
	
	@Test
	def void test8() {
		val model = parseHelper.parse('''requests: GET /<var:string>/<var2:double>/<var3:int>/<var4:boolean>;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.py"),
			'''from flask import Flask
from distutils.util import strtobool
from json import JSONEncoder

app = Flask(__name__)
URL_PREFIX = ''
HOST='0.0.0.0'
PORT=5000
@app.route(URL_PREFIX + '/<var>/<var2>/<var3>/<var4>', methods=['GET'])
def get_var_var2_var3_var4(var,var2,var3,var4):
	try:
		var2 = float(var2)
		var3 = int(var3)
		var4 = strtobool(var4) == 1
	except Exception:
		return "", 404
	#TODO
	
	return  {"test":3}


class MyEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

if __name__ == "__main__":
	app.run(debug=True, host=HOST, port=PORT)
'''.toString
		)
	}
	
	@Test
	def void test9() {
		val model = parseHelper.parse('''requests: POST /te-st/< var-a:int >;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.py"),
			'''from flask import Flask
from distutils.util import strtobool
from json import JSONEncoder

app = Flask(__name__)
URL_PREFIX = ''
HOST='0.0.0.0'
PORT=5000
@app.route(URL_PREFIX + '/te-st/<var_a>', methods=['POST'])
def post_test_vara(var_a):
	try:
		var_a = int(var_a)
	except Exception:
		return "", 404
	#TODO
	
	return  {"test":3}


class MyEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

if __name__ == "__main__":
	app.run(debug=True, host=HOST, port=PORT)
'''.toString
		)
	}
	
	@Test
	def void testOneObjectDefinedOneField() {
		val model = parseHelper.parse('''objects: Pet:id<int>;
requests:
	GET /;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.py"),
			'''from flask import Flask
from distutils.util import strtobool
from json import JSONEncoder

app = Flask(__name__)
URL_PREFIX = ''
HOST='0.0.0.0'
PORT=5000
@app.route(URL_PREFIX + '/', methods=['GET'])
def get():
	#TODO
	
	return  {"test":3}

class Pet:
	def __init__(self,id):
		self.id = id

class MyEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

if __name__ == "__main__":
	app.run(debug=True, host=HOST, port=PORT)
'''.toString
		)
	}
	
	@Test
	def void testOneObjectDefinedSeveralFields() {
		val model = parseHelper.parse('''objects: Pet:id<int>, name<string>, array<int[]>;
requests:
	GET /;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.py"),
			'''from flask import Flask
from distutils.util import strtobool
from json import JSONEncoder

app = Flask(__name__)
URL_PREFIX = ''
HOST='0.0.0.0'
PORT=5000
@app.route(URL_PREFIX + '/', methods=['GET'])
def get():
	#TODO
	
	return  {"test":3}

class Pet:
	def __init__(self,id,name,array):
		self.id = id
		self.name = name
		self.array = array

class MyEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

if __name__ == "__main__":
	app.run(debug=True, host=HOST, port=PORT)
'''.toString
		)
	}
	
	@Test
	def void testSeveralObjectDefined() {
		val model = parseHelper.parse('''objects: Pet:id<int>; Person:id<int>;
requests:
	GET /;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.py"),
			'''from flask import Flask
from distutils.util import strtobool
from json import JSONEncoder

app = Flask(__name__)
URL_PREFIX = ''
HOST='0.0.0.0'
PORT=5000
@app.route(URL_PREFIX + '/', methods=['GET'])
def get():
	#TODO
	
	return  {"test":3}

class Pet:
	def __init__(self,id):
		self.id = id
class Person:
	def __init__(self,id):
		self.id = id

class MyEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

if __name__ == "__main__":
	app.run(debug=True, host=HOST, port=PORT)
'''.toString
		)
	}
	
	@Test
	def void testResponseBodyOneField() {
		val model = parseHelper.parse('''
requests:
	GET /: response_body: {id<int>};''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.py"),
			'''from flask import Flask
from distutils.util import strtobool
from json import JSONEncoder

app = Flask(__name__)
URL_PREFIX = ''
HOST='0.0.0.0'
PORT=5000
@app.route(URL_PREFIX + '/', methods=['GET'])
def get():
	#TODO
	
	response = {
		"id" : 3 #TODO int
	}
	return MyEncoder().encode(response)


class MyEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

if __name__ == "__main__":
	app.run(debug=True, host=HOST, port=PORT)
'''.toString
		)
	}
	
	@Test
	def void testResponseBodyTwoFields() {
		val model = parseHelper.parse('''
requests:
	GET /: response_body: {id<int[]>, name<string>};''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.py"),
			'''from flask import Flask
from distutils.util import strtobool
from json import JSONEncoder

app = Flask(__name__)
URL_PREFIX = ''
HOST='0.0.0.0'
PORT=5000
@app.route(URL_PREFIX + '/', methods=['GET'])
def get():
	#TODO
	
	response = {
		"id" : 3 #TODO int[],
		"name" : 3 #TODO string
	}
	return MyEncoder().encode(response)


class MyEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

if __name__ == "__main__":
	app.run(debug=True, host=HOST, port=PORT)
'''.toString
		)
	}
}