package fr.insarennes.dsl.tests

import com.google.inject.Inject
import fr.insarennes.dsl.generator.RestGenerator
import fr.insarennes.dsl.rest.RestAPI
import org.eclipse.xtext.generator.InMemoryFileSystemAccess
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith
/**
 * Class to test the creation of the pytest file (for Integration or unit testing)
 */
@ExtendWith(InjectionExtension)
@InjectWith(typeof(RestInjectorProvider))
class ToPytestTest {
	
	var int fileNumber = 3;
	
	@Inject RestGenerator generator
	
	@Inject ParseHelper<RestAPI> parseHelper
	
	@Test
	def void test1() {
		val model = parseHelper.parse('''requests: GET /test % get %;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUTtest___synthetic0.py"),
			'''from __synthetic0 import app

PREFIX = ''

"""
% get %
"""
def test_get_test():
	#TODO
	response = app.test_client().get(PREFIX + '/test')
	assert response.status_code == 200
'''.toString
		)
	}
	
	@Test
	def void test2() {
		val model = parseHelper.parse('''requests: GET /test;


POST /
% 
post 
%;

POST /aze/aze-123AZ;

POST /a { "allow-origin":"*" };

POST /a-b { "allow-origin":"*" };

POST /aae/aze-123AZ { "Key":"Value","Key":"Value"};''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUTtest___synthetic0.py"),
			'''from __synthetic0 import app

PREFIX = ''

def test_get_test():
	#TODO
	response = app.test_client().get(PREFIX + '/test')
	assert response.status_code == 200
"""
% 
post 
%
"""
def test_post():
	#TODO
	response = app.test_client().post(PREFIX + '/')
	assert response.status_code == 200
def test_post_aze_aze123AZ():
	#TODO
	response = app.test_client().post(PREFIX + '/aze/aze-123AZ')
	assert response.status_code == 200
def test_post_a():
	#TODO
	response = app.test_client().post(PREFIX + '/a')
	assert response.status_code == 200
def test_post_ab():
	#TODO
	response = app.test_client().post(PREFIX + '/a-b')
	assert response.status_code == 200
def test_post_aae_aze123AZ():
	#TODO
	response = app.test_client().post(PREFIX + '/aae/aze-123AZ')
	assert response.status_code == 200
'''.toString
		)
	}
	
	@Test
	def void test4() {
		val model = parseHelper.parse('''requests: GET /<var:boolean>;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUTtest___synthetic0.py"),
			'''from __synthetic0 import app

PREFIX = ''

def test_get_var():
	#TODO
	response = app.test_client().get(PREFIX + '/<var>')
	assert response.status_code == 200
'''.toString
		)
	}

	@Test
	def void test5() {
		val model = parseHelper.parse('''requests: PUT /<var:string>;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUTtest___synthetic0.py"),
			'''from __synthetic0 import app

PREFIX = ''

def test_put_var():
	#TODO
	response = app.test_client().put(PREFIX + '/<var>')
	assert response.status_code == 200
'''.toString
		)
	}
	
	@Test
	def void test6() {
		val model = parseHelper.parse('''requests: POST /<var:int>;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUTtest___synthetic0.py"),
			'''from __synthetic0 import app

PREFIX = ''

def test_post_var():
	#TODO
	response = app.test_client().post(PREFIX + '/<var>')
	assert response.status_code == 200
'''.toString
		)
	}

	@Test
	def void test8() {
		val model = parseHelper.parse('''requests: GET /<var:string>/<var2:double>/<var3:int>/<var4:boolean>;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUTtest___synthetic0.py"),
			'''from __synthetic0 import app

PREFIX = ''

def test_get_var_var2_var3_var4():
	#TODO
	response = app.test_client().get(PREFIX + '/<var>/<var2>/<var3>/<var4>')
	assert response.status_code == 200
'''.toString
		)
	}
	
	@Test
	def void test7() {
		val model = parseHelper.parse('''requests: POST /te-st/< var-a:int >;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUTtest___synthetic0.py"),
			'''from __synthetic0 import app

PREFIX = ''

def test_post_test_vara():
	#TODO
	response = app.test_client().post(PREFIX + '/te-st/<var_a>')
	assert response.status_code == 200
'''.toString
		)
	}
}