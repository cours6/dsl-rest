package fr.insarennes.dsl.tests

import com.google.inject.Inject
import fr.insarennes.dsl.generator.RestGenerator
import fr.insarennes.dsl.rest.RestAPI
import org.eclipse.xtext.generator.InMemoryFileSystemAccess
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith
/**
 * Class to test the creation of the curl requests file
 */
@ExtendWith(InjectionExtension)
@InjectWith(typeof(RestInjectorProvider))
class ToCurlTest {
	
	var int fileNumber = 3;
	
	@Inject RestGenerator generator
	
	@Inject ParseHelper<RestAPI> parseHelper
	@Test
	def void test1() {
		val model = parseHelper.parse(
		'''requests: 
		GET /test % get % ;
		HEAD /anotherTest % head % ;
		POST /test : headers:{"header1":"value1", "header2":"value2"};''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.txt"),
			'''% get %
curl -X GET localhost:5000/test
% head %
curl -I localhost:5000/anotherTest
curl -H "header1: value1" -H "header2: value2" -X POST localhost:5000/test
'''.toString)
	}
	
	@Test
	def void test2() {
		val model = parseHelper.parse(
		'''address: aze;
requests: GET /;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.txt"),
			'''curl -X GET aze:5000/
'''.toString)
	}
	
	@Test
	def void test3() {
		val model = parseHelper.parse(
		'''address: aze; port: 1234;
requests: GET /;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.txt"),
			'''curl -X GET aze:1234/
'''.toString)
	}
	
	@Test
	def void test4() {
		val model = parseHelper.parse(
		'''port: 1234;
requests: GET /;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.txt"),
			'''curl -X GET localhost:1234/
'''.toString)
	}
	
	@Test
	def void test5() {
		val model = parseHelper.parse(
		'''requests: GET /<var:boolean>;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.txt"),
			'''curl -X GET localhost:5000/<var:boolean>
'''.toString)
	}
	
	@Test
	def void test6() {
		val model = parseHelper.parse(
		'''requests: GET /<var:int>;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.txt"),
			'''curl -X GET localhost:5000/<var:int>
'''.toString)
	}
	
	@Test
	def void test7() {
		val model = parseHelper.parse(
		'''requests: GET /<var:double>;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.txt"),
			'''curl -X GET localhost:5000/<var:double>
'''.toString)
	}
	
	@Test
	def void test8() {
		val model = parseHelper.parse(
		'''requests: GET /<var:string>;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.txt"),
			'''curl -X GET localhost:5000/<var:string>
'''.toString)
	}
	
	@Test
	def void test9() {
		val model = parseHelper.parse(
		'''requests: GET /<var:string>/<var2:double>/<var3:int>/<var4:boolean>/;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.txt"),
			'''curl -X GET localhost:5000/<var:string>/<var2:double>/<var3:int>/<var4:boolean>/
'''.toString)
	}
	
	@Test
	def void test10() {
		val model = parseHelper.parse('''requests: POST /te-st/< var-a:int >;''')
		val fsa = new InMemoryFileSystemAccess()
		generator.doGenerate(model.eResource, fsa, null)

		Assertions.assertEquals(fileNumber, fsa.textFiles.size)
		Assertions.assertEquals(fsa.textFiles.get("DEFAULT_OUTPUT__synthetic0.txt"),
			'''curl -X POST localhost:5000/te-st/<var-a:int>
'''.toString
		)
	}
}